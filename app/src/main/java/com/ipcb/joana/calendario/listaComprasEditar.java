package com.ipcb.joana.calendario;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;

public class listaComprasEditar extends AppCompatActivity {

    private static final int REQUEST_CODE = 43;

    private EditText titulo, nome_anexo, descricao;
    private Button btnData, btnHora, btnGuardar, btnProcurar, btnEliminar;
    private TextView tData, tHora;
    private int dia, mes, ano, hora, minutos, dia_escolhido, mes_escolhido, ano_escolhido, hora_escolhida, minutos_escolhidos;
    private Calendar calendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener datePickerListener;
    private TimePickerDialog.OnTimeSetListener timePickerListener;
    private String path;
    private Tarefa tarefa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_compras_editar);

        btnData = (Button) findViewById(R.id.btndata);
        btnHora = (Button) findViewById(R.id.btnhora);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        tData = (TextView) findViewById(R.id.tData);
        tHora = (TextView) findViewById(R.id.tHora);
        titulo = (EditText) findViewById(R.id.titulo);
        nome_anexo = (EditText) findViewById(R.id.nome_anexo);
        descricao = (EditText) findViewById(R.id.descricao);
        btnProcurar = (Button) findViewById(R.id.btnProcurar);
        btnEliminar = (Button) findViewById(R.id.btnEliminar);

        mostrarDados();

        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH);
        ano = calendar.get(Calendar.YEAR);

        hora = calendar.get(Calendar.HOUR_OF_DAY);
        minutos = calendar.get(Calendar.MINUTE);

        /*Data e hora pickers*/
        btnData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog date = new DatePickerDialog(listaComprasEditar.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, datePickerListener, ano, mes, dia);
                date.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                date.show();
            }
        });

        datePickerListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int ano, int mes, int dia) {
                tData.setText(dia + "/" + (mes + 1) + "/" + ano);

                dia_escolhido = dia;
                mes_escolhido = (mes + 1);
                ano_escolhido = ano;
            }
        };

        btnHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog time = new TimePickerDialog(listaComprasEditar.this, timePickerListener, hora, minutos, true);
                time.show();
            }
        });

        timePickerListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hora, int minuto) {
                if (minuto < 10)
                    tHora.setText(hora + ":0" + minuto);
                else
                    tHora.setText(hora + ":" + minuto);

                hora_escolhida = hora;
                minutos_escolhidos = minuto;
            }
        };
        /*Data e hora pickers*/

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(titulo.getText().toString().length() > 0) {
                    tarefa.setTitulo(titulo.getText().toString());
                    tarefa.setData(tData.getText().toString());
                    tarefa.setHora(tHora.getText().toString());
                    if(path != "")
                        tarefa.setAnexo(path);
                    tarefa.setDescricao(descricao.getText().toString());

                    AgendaDBHandler db = new AgendaDBHandler(listaComprasEditar.this);
                    db.updateTarefa(tarefa);

                    Toast.makeText(getApplicationContext(), "Tarefa editada com sucesso", Toast.LENGTH_SHORT).show();

                    Intent calendario = new Intent(listaComprasEditar.this, MainActivity.class);
                    startActivity(calendario);
                } else {
                    Toast.makeText(getApplicationContext(), "Introduza o titulo", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnProcurar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent procurar = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                procurar.setType("image/*");
                procurar.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(procurar, REQUEST_CODE);
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgendaDBHandler db = new AgendaDBHandler(listaComprasEditar.this);
                db.deleteTarefa(tarefa);

                Toast.makeText(getApplicationContext(), "Tarefa eliminada com sucesso", Toast.LENGTH_SHORT).show();

                Intent calendario = new Intent(listaComprasEditar.this, MainActivity.class);
                startActivity(calendario);
            }
        });

        nome_anexo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(tarefa.getAnexo());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "image/*");

                if(intent.resolveActivity(getPackageManager()) != null)
                    startActivity(intent);
                else
                    Toast.makeText(getApplicationContext(), "Não tem uma aplicação para abrir pdf!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void mostrarDados() {
        Bundle extrasBundle = getIntent().getExtras();

        if (extrasBundle != null && !extrasBundle.isEmpty()) {
            tarefa = new AgendaDBHandler(listaComprasEditar.this).getTarefa(extrasBundle.getInt("idTarefa"), 1);

            String Titulo = tarefa.getTitulo();

            titulo.setText(Titulo);
            tData.setText(tarefa.getData());
            tHora.setText(tarefa.getHora());
            nome_anexo.setText(tarefa.getAnexo().substring(tarefa.getAnexo().lastIndexOf("/") + 1));
            descricao.setText(tarefa.getDescricao());
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK){
            if(data != null){
                Uri uri = data.getData();

                path = uri.getPath();

                String filename = uri.getPath().substring(uri.getPath().lastIndexOf("/")+1);
                nome_anexo.setText(filename);
            }
        }
    }
}
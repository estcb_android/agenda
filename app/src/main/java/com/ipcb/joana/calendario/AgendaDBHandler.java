package com.ipcb.joana.calendario;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AgendaDBHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Agenda.db";

     /*
    Tipo de tarefa:

    0 - Reunião
    1 - Lista de compras
    2 - Marcação
    3 - Aniversário

     */

      /*
    Status:

    0 - Completa
    1 - Não completa

     */

    //Tabela tarefa
    private static final String TABLE_TAREFA = "tarefa";
    private static final String TAREFA_IDTAREFA = "idTarefa";
    private static final String TAREFA_TITULO = "titulo";
    private static final String TAREFA_DATA = "data";
    private static final String TAREFA_HORA = "hora";
    private static final String TAREFA_STATUS = "status";
    private static final String TAREFA_TIPO = "tipo";

    //Tabela reunião
    private static final String TABLE_REUNIAO = "reuniao";
    private static final String REUNIAO_IDREUNIAO = "idReuniao";
    private static final String REUNIAO_DESCRICAO = "descricao";
    private static final String REUNIAO_ANEXO = "anexo";
    private static final String REUNIAO_IDTAREFA = "idTarefa";

    //Tabela lista de compras
    private static final String TABLE_LISTA = "lista";
    private static final String LISTA_IDLISTA = "idLista";
    private static final String LISTA_DESCRICAO = "descricao";
    private static final String LISTA_ANEXO = "anexo";
    private static final String LISTA_IDTAREFA = "idTarefa";

    //Tabela marcação
    private static final String TABLE_MARCACAO = "marcacao";
    private static final String MARCACAO_IDMARCACAO = "idMarcacao";
    private static final String MARCACAO_DESCRICAO = "descricao";
    private static final String MARCACAO_ESTRELAS = "estrelas";
    private static final String MARCACAO_IDTAREFA = "idTarefa";

    //Tabela aniversário
    private static final String TABLE_ANIVERSARIO = "aniversario";
    private static final String ANIVERSARIO_IDANIVERSARIO = "idAniversario";
    private static final String ANIVERSARIO_CONTACTO = "contacto";
    private static final String ANIVERSARIO_MENSAGEM = "mensagem";
    private static final String ANIVERSARIO_IDTAREFA = "idTarefa";



    private int dia, mes, ano;
    private Calendar calendar = Calendar.getInstance();

    public AgendaDBHandler(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Tabela tarefa
        db.execSQL("CREATE TABLE " + TABLE_TAREFA + "(" + TAREFA_IDTAREFA + " integer primary key autoincrement, " + TAREFA_TITULO + " text, " + TAREFA_DATA + " text, " + TAREFA_HORA + " text, " + TAREFA_STATUS + " integer, " + TAREFA_TIPO + " integer)");

        //Tabela reunião
        db.execSQL("CREATE TABLE " + TABLE_REUNIAO + "(" + REUNIAO_IDREUNIAO + " integer primary key autoincrement, " + REUNIAO_DESCRICAO + " text, " + REUNIAO_ANEXO + " text, " + REUNIAO_IDTAREFA + " integer, foreign key (" + REUNIAO_IDTAREFA + ") references " + TABLE_TAREFA + "(" + TAREFA_IDTAREFA + "));");

        //Tabela lista de compras
        db.execSQL("CREATE TABLE " + TABLE_LISTA + "(" + LISTA_IDLISTA + " integer primary key autoincrement, " + LISTA_DESCRICAO + " text, " + LISTA_ANEXO + " text, " + LISTA_IDTAREFA + " integer, foreign key (" + LISTA_IDTAREFA + ") references " + TABLE_TAREFA + "(" + TAREFA_IDTAREFA + "));");

        //Tabela marcação
        db.execSQL("CREATE TABLE " + TABLE_MARCACAO + "(" + MARCACAO_IDMARCACAO + " integer primary key autoincrement, " + MARCACAO_DESCRICAO + " text, " + MARCACAO_ESTRELAS + " integer, " + MARCACAO_IDTAREFA + " integer, foreign key (" + MARCACAO_IDTAREFA + ") references " + TABLE_TAREFA + "(" + TAREFA_IDTAREFA + "));");

        //Tabela aniversário
        db.execSQL("CREATE TABLE " + TABLE_ANIVERSARIO + "(" + ANIVERSARIO_IDANIVERSARIO + " integer primary key autoincrement, " + ANIVERSARIO_CONTACTO + " integer, " + ANIVERSARIO_MENSAGEM + " text, " + ANIVERSARIO_IDTAREFA + " integer, foreign key (" + ANIVERSARIO_IDTAREFA + ") references " + TABLE_TAREFA + "(" + TAREFA_IDTAREFA + "));");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Tabela tarefa
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAREFA);

        //Tabela reunião
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REUNIAO);

        //Tabela lista de compras
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LISTA);

        //Tabela marcação
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MARCACAO);

        //Tabela aniversário
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ANIVERSARIO);

        onCreate(db);

    }

    public void addTarefa(Tarefa tarefa) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues valuesTarefa = new ContentValues();
        valuesTarefa.put(TAREFA_TITULO, tarefa.getTitulo());
        valuesTarefa.put(TAREFA_DATA, tarefa.getData());
        valuesTarefa.put(TAREFA_HORA, tarefa.getHora());
        valuesTarefa.put(TAREFA_STATUS, tarefa.getStatus());
        valuesTarefa.put(TAREFA_TIPO, tarefa.getTipo());

        db.insert(TABLE_TAREFA, null, valuesTarefa);

        int tipo = tarefa.getTipo();

        String selectIdTarefa = "SELECT "+TAREFA_IDTAREFA+" FROM " +TABLE_TAREFA+ " WHERE " +TAREFA_TITULO+ " = '"+tarefa.getTitulo()+"' AND "+TAREFA_DATA+" = '"+tarefa.getData()+"' AND "+TAREFA_HORA+" = '"+tarefa.getHora()+"' AND "+TAREFA_STATUS+" = "+tarefa.getStatus()+" AND "+TAREFA_TIPO+" = "+tarefa.getTipo();

        Cursor cursorTarefa = db.rawQuery(selectIdTarefa, null);

        if (cursorTarefa != null)
            cursorTarefa.moveToFirst();

        tarefa.setId(Integer.parseInt(cursorTarefa.getString(0)));

        switch (tipo) {
            case 0:

                ContentValues valuesReuniao = new ContentValues();
                valuesReuniao.put(REUNIAO_DESCRICAO, tarefa.getDescricao());
                valuesReuniao.put(REUNIAO_ANEXO, tarefa.getAnexo());
                valuesReuniao.put(REUNIAO_IDTAREFA, tarefa.getId());

                db.insert(TABLE_REUNIAO, null, valuesReuniao);

                break;

            case 1:

                ContentValues valuesLista = new ContentValues();
                valuesLista.put(LISTA_DESCRICAO, tarefa.getDescricao());
                valuesLista.put(LISTA_ANEXO, tarefa.getAnexo());
                valuesLista.put(LISTA_IDTAREFA, tarefa.getId());

                db.insert(TABLE_LISTA, null, valuesLista);

                break;

            case 2:

                ContentValues valuesMarcacao = new ContentValues();
                valuesMarcacao.put(MARCACAO_DESCRICAO, tarefa.getDescricao());
                valuesMarcacao.put(MARCACAO_ESTRELAS, tarefa.getEstrelas());
                valuesMarcacao.put(MARCACAO_IDTAREFA, tarefa.getId());

                db.insert(TABLE_MARCACAO, null, valuesMarcacao);

                break;

            case 3:

                ContentValues valuesAniversario = new ContentValues();
                valuesAniversario.put(ANIVERSARIO_CONTACTO, tarefa.getContacto());
                valuesAniversario.put(ANIVERSARIO_MENSAGEM, tarefa.getMensagem());
                valuesAniversario.put(ANIVERSARIO_IDTAREFA, tarefa.getId());

                db.insert(TABLE_ANIVERSARIO, null, valuesAniversario);

                break;

            default:

                break;
        }

        db.close();

    }

    public Tarefa getTarefa(int id, int tipo) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursorTarefa = db.query(TABLE_TAREFA, new String[]{TAREFA_IDTAREFA, TAREFA_TITULO, TAREFA_DATA, TAREFA_HORA, TAREFA_STATUS, TAREFA_TIPO}, TAREFA_IDTAREFA + "=?", new String[]{String.valueOf(id)}, null, null, null);

        if (cursorTarefa != null)
            cursorTarefa.moveToFirst();

        switch (tipo) {
            case 0:

                Cursor cursorReuniao = db.query(TABLE_REUNIAO, new String[]{REUNIAO_IDREUNIAO, REUNIAO_DESCRICAO, REUNIAO_ANEXO, REUNIAO_IDTAREFA}, REUNIAO_IDTAREFA + "=?", new String[]{String.valueOf(id)}, null, null, null);

                if (cursorReuniao != null)
                    cursorReuniao.moveToFirst();

                Tarefa tarefaReuniao = new Tarefa(Integer.parseInt(cursorTarefa.getString(0)), cursorTarefa.getString(1), cursorTarefa.getString(2), cursorTarefa.getString(3), Integer.parseInt(cursorTarefa.getString(4)), Integer.parseInt(cursorTarefa.getString(5)), cursorReuniao.getString(1), cursorReuniao.getString(2));

                return tarefaReuniao;

            case 1:

                Cursor cursorLista = db.query(TABLE_LISTA, new String[]{LISTA_IDLISTA, LISTA_DESCRICAO, LISTA_ANEXO, LISTA_IDTAREFA}, LISTA_IDTAREFA + "=?", new String[]{String.valueOf(id)}, null, null, null);

                if (cursorLista != null)
                    cursorLista.moveToFirst();

                Tarefa tarefaLista = new Tarefa(Integer.parseInt(cursorTarefa.getString(0)), cursorTarefa.getString(1), cursorTarefa.getString(2), cursorTarefa.getString(3), Integer.parseInt(cursorTarefa.getString(4)), Integer.parseInt(cursorTarefa.getString(5)), cursorLista.getString(1), cursorLista.getString(2));

                return tarefaLista;

            case 2:

                Cursor cursorMarcacao = db.query(TABLE_MARCACAO, new String[]{MARCACAO_IDMARCACAO, MARCACAO_DESCRICAO, MARCACAO_ESTRELAS, MARCACAO_IDTAREFA}, MARCACAO_IDTAREFA + "=?", new String[]{String.valueOf(id)}, null, null, null);

                if (cursorMarcacao != null)
                    cursorMarcacao.moveToFirst();

                Tarefa tarefaMarcacao = new Tarefa(Integer.parseInt(cursorTarefa.getString(0)), cursorTarefa.getString(1), cursorTarefa.getString(2), cursorTarefa.getString(3), Integer.parseInt(cursorTarefa.getString(4)), Integer.parseInt(cursorTarefa.getString(5)), cursorMarcacao.getString(1), Integer.parseInt(cursorMarcacao.getString(2)));

                return tarefaMarcacao;

            case 3:

                Cursor cursorAniversario = db.query(TABLE_ANIVERSARIO, new String[]{ANIVERSARIO_IDANIVERSARIO, ANIVERSARIO_CONTACTO, ANIVERSARIO_MENSAGEM, ANIVERSARIO_IDTAREFA}, ANIVERSARIO_IDTAREFA + "=?", new String[]{String.valueOf(id)}, null, null, null);

                if (cursorAniversario != null)
                    cursorAniversario.moveToFirst();

                Tarefa tarefaAniversario = new Tarefa(Integer.parseInt(cursorTarefa.getString(0)), cursorTarefa.getString(1), cursorTarefa.getString(2), cursorTarefa.getString(3), Integer.parseInt(cursorTarefa.getString(4)), Integer.parseInt(cursorTarefa.getString(5)), Integer.parseInt(cursorAniversario.getString(1)), cursorAniversario.getString(2));

                return tarefaAniversario;

            default:

                break;
        }

        return null;
    }

    public ArrayList<Tarefa> getHojeTarefas() {
        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH);
        ano = calendar.get(Calendar.YEAR);

        ArrayList<Tarefa> listaTarefas = new ArrayList<Tarefa>();

        String selectQuery = "SELECT * FROM " + TABLE_TAREFA;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                if(cursor.getString(2).equals((dia + "/" + (mes + 1) + "/" + ano))) {
                    Tarefa tarefa = new Tarefa();

                    tarefa.setId(Integer.parseInt(cursor.getString(0)));
                    tarefa.setTitulo(cursor.getString(1));
                    tarefa.setData(cursor.getString(2));
                    tarefa.setHora(cursor.getString(3));
                    tarefa.setStatus(Integer.parseInt(cursor.getString(4)));
                    tarefa.setTipo(Integer.parseInt(cursor.getString(5)));

                    int tipo = Integer.parseInt(cursor.getString(5));
                    int id = Integer.parseInt(cursor.getString(0));

                    switch (tipo) {
                        case 0:

                            String selectReuniao = "SELECT * FROM " + TABLE_REUNIAO + " WHERE " + REUNIAO_IDTAREFA + " = " + id;

                            Cursor cursorReuniao = db.rawQuery(selectReuniao, null);

                            if (cursorReuniao != null)
                                cursorReuniao.moveToFirst();

                            tarefa.setDescricao(cursorReuniao.getString(1));
                            tarefa.setAnexo(cursorReuniao.getString(2));

                            break;

                        case 1:

                            String selectLista = "SELECT * FROM " + TABLE_LISTA + " WHERE " + LISTA_IDTAREFA + " = " + id;

                            Cursor cursorLista = db.rawQuery(selectLista, null);

                            if (cursorLista != null)
                                cursorLista.moveToFirst();

                            tarefa.setDescricao(cursorLista.getString(1));
                            tarefa.setAnexo(cursorLista.getString(2));

                            break;

                        case 2:

                            String selectMarcacao = "SELECT * FROM " + TABLE_MARCACAO + " WHERE " + MARCACAO_IDTAREFA + " = " + id;

                            Cursor cursorMarcacao = db.rawQuery(selectMarcacao, null);

                            if (cursorMarcacao != null)
                                cursorMarcacao.moveToFirst();

                            tarefa.setDescricao(cursorMarcacao.getString(1));
                            tarefa.setEstrelas(Integer.parseInt(cursorMarcacao.getString(2)));

                            break;

                        case 3:

                            String selectAniversario = "SELECT * FROM " + TABLE_ANIVERSARIO + " WHERE " + ANIVERSARIO_IDTAREFA + " = " + id;

                            Cursor cursorAniversario = db.rawQuery(selectAniversario, null);

                            if (cursorAniversario != null)
                                cursorAniversario.moveToFirst();

                            tarefa.setContacto(Integer.parseInt(cursorAniversario.getString(1)));
                            tarefa.setMensagem(cursorAniversario.getString(2));

                            break;

                        default:

                            break;
                    }

                    listaTarefas.add(tarefa);
                }

            } while (cursor.moveToNext());
        }

        return listaTarefas;

    }

    public ArrayList<Tarefa> getHojeAniversarios() {
        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH);
        ano = calendar.get(Calendar.YEAR);

        ArrayList<Tarefa> listaTarefas = new ArrayList<Tarefa>();

        String selectQuery = "SELECT * FROM " + TABLE_TAREFA;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                if(cursor.getString(2).equals((dia + "/" + (mes + 1) + "/" + ano))) {
                    Log.v("DATA",(dia + "/" + (mes + 1) + "/" + ano));
                    Tarefa tarefa = new Tarefa();

                    tarefa.setId(Integer.parseInt(cursor.getString(0)));
                    tarefa.setTitulo(cursor.getString(1));
                    tarefa.setData(cursor.getString(2));
                    tarefa.setHora(cursor.getString(3));
                    tarefa.setStatus(Integer.parseInt(cursor.getString(4)));
                    tarefa.setTipo(Integer.parseInt(cursor.getString(5)));

                    int tipo = Integer.parseInt(cursor.getString(5));
                    int id = Integer.parseInt(cursor.getString(0));

                    switch (tipo) {
                        case 3:

                            String selectAniversario = "SELECT * FROM " + TABLE_ANIVERSARIO + " WHERE " + ANIVERSARIO_IDTAREFA + " = " + id;

                            Cursor cursorAniversario = db.rawQuery(selectAniversario, null);

                            if (cursorAniversario != null)
                                cursorAniversario.moveToFirst();

                            tarefa.setContacto(Integer.parseInt(cursorAniversario.getString(1)));
                            tarefa.setMensagem(cursorAniversario.getString(2));

                            break;

                        default:

                            break;
                    }

                    listaTarefas.add(tarefa);
                }

            } while (cursor.moveToNext());
        }

        return listaTarefas;

    }

    public int updateTarefa(Tarefa tarefa) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues valuesTarefa = new ContentValues();
        valuesTarefa.put(TAREFA_TITULO, tarefa.getTitulo());
        valuesTarefa.put(TAREFA_DATA, tarefa.getData());
        valuesTarefa.put(TAREFA_HORA, tarefa.getHora());
        valuesTarefa.put(TAREFA_STATUS, tarefa.getStatus());
        valuesTarefa.put(TAREFA_TIPO, tarefa.getTipo());

        int tipo = tarefa.getTipo();

        switch (tipo) {
            case 0:

                ContentValues valuesReuniao = new ContentValues();
                valuesReuniao.put(REUNIAO_DESCRICAO, tarefa.getDescricao());
                valuesReuniao.put(REUNIAO_ANEXO, tarefa.getAnexo());
                valuesReuniao.put(REUNIAO_IDTAREFA, tarefa.getId());

                db.update(TABLE_REUNIAO, valuesReuniao, REUNIAO_IDTAREFA + " = ?", new String[]{String.valueOf(tarefa.getId())});

                break;

            case 1:

                ContentValues valuesLista = new ContentValues();
                valuesLista.put(LISTA_DESCRICAO, tarefa.getDescricao());
                valuesLista.put(LISTA_ANEXO, tarefa.getAnexo());
                valuesLista.put(LISTA_IDTAREFA, tarefa.getId());

                db.update(TABLE_LISTA, valuesLista, LISTA_IDTAREFA + " = ?", new String[]{String.valueOf(tarefa.getId())});

                break;

            case 2:

                ContentValues valuesMarcacao = new ContentValues();
                valuesMarcacao.put(MARCACAO_DESCRICAO, tarefa.getDescricao());
                valuesMarcacao.put(MARCACAO_ESTRELAS, tarefa.getEstrelas());
                valuesMarcacao.put(MARCACAO_IDTAREFA, tarefa.getId());

                db.update(TABLE_MARCACAO, valuesMarcacao, MARCACAO_IDTAREFA + " = ?", new String[]{String.valueOf(tarefa.getId())});

                break;

            case 3:

                ContentValues valuesAniversario = new ContentValues();
                valuesAniversario.put(ANIVERSARIO_CONTACTO, tarefa.getContacto());
                valuesAniversario.put(ANIVERSARIO_MENSAGEM, tarefa.getMensagem());
                valuesAniversario.put(ANIVERSARIO_IDTAREFA, tarefa.getId());

                db.update(TABLE_ANIVERSARIO, valuesAniversario, ANIVERSARIO_IDTAREFA + " = ?", new String[]{String.valueOf(tarefa.getId())});

                break;

            default:

                break;
        }

        return db.update(TABLE_TAREFA, valuesTarefa, TAREFA_IDTAREFA + " = ?", new String[]{String.valueOf(tarefa.getId())});

    }

    public void deleteTarefa(Tarefa tarefa) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_TAREFA, TAREFA_IDTAREFA + " = ?", new String[]{String.valueOf(tarefa.getId())});

        int tipo = tarefa.getTipo();

        switch (tipo) {
            case 0:

                db.delete(TABLE_REUNIAO, REUNIAO_IDTAREFA + " = ?", new String[]{String.valueOf(tarefa.getId())});

                break;

            case 1:

                db.delete(TABLE_LISTA, LISTA_IDTAREFA + " = ?", new String[]{String.valueOf(tarefa.getId())});

                break;

            case 2:

                db.delete(TABLE_MARCACAO, MARCACAO_IDTAREFA + " = ?", new String[]{String.valueOf(tarefa.getId())});

                break;

            case 3:

                db.delete(TABLE_ANIVERSARIO, ANIVERSARIO_IDTAREFA + " = ?", new String[]{String.valueOf(tarefa.getId())});

                break;

            default:

                break;
        }

        db.close();
    }
}
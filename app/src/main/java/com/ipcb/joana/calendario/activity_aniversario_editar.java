package com.ipcb.joana.calendario;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class activity_aniversario_editar extends AppCompatActivity {
    private static final int REQUEST_CODE = 1;

    private EditText titulo, mensagemPers;
    private Button btnData, btnHora, btnGuardar, btnProcurar, btnEliminar;
    private TextView tData, tHora, contacto, mensagemPadrao;
    private int dia, mes, ano, hora, minutos, dia_escolhido, mes_escolhido, ano_escolhido, hora_escolhida, minutos_escolhidos, numero = 0;
    private Calendar calendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener datePickerListener;
    private TimePickerDialog.OnTimeSetListener timePickerListener;
    private String mensagem;
    private CheckBox checkBox_mensagemPers, checkBox_mensagemPadrao;
    private Tarefa tarefa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aniversario_editar);

        btnData = (Button) findViewById(R.id.btndata);
        btnHora = (Button) findViewById(R.id.btnhora);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        tData = (TextView) findViewById(R.id.tData);
        tHora = (TextView) findViewById(R.id.tHora);
        titulo = (EditText) findViewById(R.id.titulo);
        btnProcurar = (Button) findViewById(R.id.btnProcurar);
        contacto = (TextView) findViewById(R.id.contacto);
        checkBox_mensagemPers = (CheckBox) findViewById(R.id.checkBox_mensagem);
        mensagemPers = (EditText) findViewById(R.id.mensagemPers);
        mensagemPadrao = (TextView) findViewById(R.id.mensagemPadrao);
        btnEliminar = (Button) findViewById(R.id.btnEliminar);
        checkBox_mensagemPadrao = (CheckBox) findViewById(R.id.checkBox);

        mostrarDados();

        if(checkBox_mensagemPers.isChecked()){
            mensagem = mensagemPers.getText().toString();
        }else{
            mensagem = mensagemPadrao.getText().toString();
        }

        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH);
        ano = calendar.get(Calendar.YEAR);
        
        hora = calendar.get(Calendar.HOUR_OF_DAY);
        minutos = calendar.get(Calendar.MINUTE);

        /*Data e hora pickers*/
        btnData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog date = new DatePickerDialog(activity_aniversario_editar.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, datePickerListener, ano, mes, dia);
                date.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                date.show();
            }
        });

        datePickerListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int ano, int mes, int dia) {
                tData.setText(dia + "/" + (mes + 1) + "/" + ano);

                dia_escolhido = dia;
                mes_escolhido = (mes + 1);
                ano_escolhido = ano;
            }
        };

        btnHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog time = new TimePickerDialog(activity_aniversario_editar.this, timePickerListener, hora, minutos, true);
                time.show();
            }
        });

        timePickerListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hora, int minuto) {
                if (minuto < 10)
                    tHora.setText(hora + ":0" + minuto);
                else
                    tHora.setText(hora + ":" + minuto);

                hora_escolhida = hora;
                minutos_escolhidos = minuto;
            }
        };
        /*Data e hora pickers*/

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(titulo.getText().toString().length() > 0) {
                    tarefa.setTitulo(titulo.getText().toString());
                    tarefa.setData(tData.getText().toString());
                    tarefa.setHora(tHora.getText().toString());
                    /*Retirar todas as letras do contacto*/
                    String numero = contacto.getText().toString().replaceAll("\\D+","");
                    if (!numero.equals(Integer.toString(tarefa.getContacto()))) {
                        tarefa.setContacto(Integer.parseInt(numero));
                    }
                    if (checkBox_mensagemPadrao.isChecked())
                        tarefa.setMensagem(mensagemPadrao.getText().toString());
                    else
                        tarefa.setMensagem(mensagemPers.getText().toString());

                    AgendaDBHandler db = new AgendaDBHandler(activity_aniversario_editar.this);
                    db.updateTarefa(tarefa);

                    Toast.makeText(getApplicationContext(), "Tarefa editada com sucesso", Toast.LENGTH_SHORT).show();

                    Intent calendario = new Intent(activity_aniversario_editar.this, MainActivity.class);
                    startActivity(calendario);
                }else{
                    Toast.makeText(getApplicationContext(), "Introduza o titulo", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnProcurar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(i, REQUEST_CODE);
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgendaDBHandler db = new AgendaDBHandler(activity_aniversario_editar.this);
                db.deleteTarefa(tarefa);

                Toast.makeText(getApplicationContext(), "Tarefa eliminada com sucesso", Toast.LENGTH_SHORT).show();

                Intent calendario = new Intent(activity_aniversario_editar.this, MainActivity.class);
                startActivity(calendario);
            }
        });
    }

    private void mostrarDados() {
        Bundle extrasBundle = getIntent().getExtras();

        if (extrasBundle != null && !extrasBundle.isEmpty()) {
            tarefa = new AgendaDBHandler(activity_aniversario_editar.this).getTarefa(extrasBundle.getInt("idTarefa"), 3);

            String Titulo = tarefa.getTitulo();

            titulo.setText(Titulo);
            tData.setText(tarefa.getData());
            tHora.setText(tarefa.getHora());
            if(tarefa.getMensagem().equals("Muitos parabéns! Beijinhos")) {
                checkBox_mensagemPadrao.setChecked(true);
                checkBox_mensagemPers.setChecked(false);
            }else{
                checkBox_mensagemPadrao.setChecked(false);
                checkBox_mensagemPers.setChecked(true);
                mensagemPers.setText(tarefa.getMensagem());
            }
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK){
            if(data != null){
                Uri contactUri = data.getData();
                Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);
                cursor.moveToFirst();
                int nome = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                numero = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                contacto.setText(cursor.getString(nome)+" ("+cursor.getString(numero)+")");
            }
        }
    }
}

package com.ipcb.joana.calendario;

import android.Manifest;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class activityEnviarMensagem extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkForSmsPermission();
    }

    private void checkForSmsPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);
        }else{
            enviarMensagem();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (permissions[0].equalsIgnoreCase(Manifest.permission.SEND_SMS) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enviarMensagem();
                } else {
                    Toast.makeText(getApplicationContext(), "Caso tenha uma tarefa aniversário, a mensagem não vai ser enviada!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void enviarMensagem() {
        Bundle dados = getIntent().getExtras();

        String scAddress = null;

        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(Integer.toString(dados.getInt("contacto")), scAddress, dados.getString("mensagem"), null, null);

        Toast.makeText(getApplicationContext(), "Mensagem enviada!", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}

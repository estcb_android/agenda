package com.ipcb.joana.calendario;

public class Tarefa {

    //Atributos da tabela tarefa
    private int id;
    private String titulo;
    private String data;
    private String hora;
    private int status;

    /*
    Tipo de tarefa:

    0 - Reunião
    1 - Lista de compras
    2 - Marcação
    3 - Aniversário

     */
    private int tipo;

    //Atributos das tabelas reunião e lista de compras
    private String descricao;
    private String anexo;

    //Atributos da tabela marcação
    private int estrelas;

    //Atributos da tabela aniversário
    private int contacto;
    private String mensagem;

    public Tarefa(){}

    //Contrutor para uma tarefa do tipo reunião e lista de compras
    public Tarefa(int id, String titulo, String data, String hora, int status, int tipo, String descricao, String anexo){

        this.id = id;
        this.titulo = titulo;
        this.data = data;
        this.hora = hora;
        this.status = status;
        this.tipo = tipo;
        this.descricao = descricao;
        this.anexo = anexo;

    }
    public Tarefa(String titulo, String data, String hora, int status, int tipo, String descricao, String anexo){

        this.titulo = titulo;
        this.data = data;
        this.hora = hora;
        this.status = status;
        this.tipo = tipo;
        this.descricao = descricao;
        this.anexo = anexo;

    }

    //Construtor para uma tarefa do tipo marcação
    public Tarefa(int id, String titulo, String data, String hora, int status, int tipo, String descricao, int estrelas){

        this.id = id;
        this.titulo = titulo;
        this.data = data;
        this.hora = hora;
        this.status = status;
        this.tipo = tipo;
        this.descricao = descricao;
        this.estrelas = estrelas;

    }
    public Tarefa(String titulo, String data, String hora, int status, int tipo, String descricao, int estrelas){

        this.titulo = titulo;
        this.data = data;
        this.hora = hora;
        this.status = status;
        this.tipo = tipo;
        this.descricao = descricao;
        this.estrelas = estrelas;

    }

    //Construtor para uma tarefa do tipo aniversário
    public Tarefa(int id, String titulo, String data, String hora, int status, int tipo, int contacto, String mensagem){

        this.id = id;
        this.titulo = titulo;
        this.data = data;
        this.hora = hora;
        this.status = status;
        this.tipo = tipo;
        this.contacto = contacto;
        this.mensagem = mensagem;

    }
    public Tarefa(String titulo, String data, String hora, int status, int tipo, int contacto, String mensagem){

        this.titulo = titulo;
        this.data = data;
        this.hora = hora;
        this.status = status;
        this.tipo = tipo;
        this.contacto = contacto;
        this.mensagem = mensagem;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTipo() { return tipo; }

    public void setTipo(int tipo) { this.tipo = tipo; }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public int getEstrelas() {
        return estrelas;
    }

    public void setEstrelas(int estrelas) {
        this.estrelas = estrelas;
    }

    public int getContacto() { return contacto; }

    public void setContacto(int contacto) {
        this.contacto = contacto;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
package com.ipcb.joana.calendario;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class tipoEvento extends AppCompatActivity {

    Button btnVoltar, btnReuniao, btnLista, btnMarcacao, btnAniversario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_evento);

        btnVoltar = (Button) findViewById(R.id.btnVoltar);
        btnReuniao = (Button) findViewById(R.id.btnReuniao);
        btnLista = (Button) findViewById(R.id.btnLista);
        btnMarcacao = (Button) findViewById(R.id.btnMarcacao);
        btnAniversario = (Button) findViewById(R.id.btnAniversario);

        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent voltar = new Intent(tipoEvento.this, MainActivity.class);
                startActivity(voltar);
            }
        });

        btnReuniao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reuniao = new Intent(tipoEvento.this, reuniao.class);
                startActivity(reuniao);
            }
        });

        btnLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent lista = new Intent(tipoEvento.this, listaCompras.class);
                startActivity(lista);
            }
        });

        btnMarcacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent marcacao = new Intent(tipoEvento.this, marcacao.class);
                startActivity(marcacao);
            }
        });

        btnAniversario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent aniversario = new Intent(tipoEvento.this, activity_aniversario.class);
                startActivity(aniversario);
            }
        });
    }
}

package com.ipcb.joana.calendario;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class marcacao extends AppCompatActivity {

    private EditText titulo, nome_anexo, descricao;
    private Spinner opcoes;
    private Button btnData, btnHora, btnGuardar;
    private TextView tData, tHora;
    private int dia, mes, ano, hora, minutos, dia_escolhido, mes_escolhido, ano_escolhido, hora_escolhida, minutos_escolhidos;
    private Calendar calendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener datePickerListener;
    private TimePickerDialog.OnTimeSetListener timePickerListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marcacao);

        opcoes = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.opcoes_marcacaolayout, android.R.layout.simple_spinner_item);
        opcoes.setAdapter(adapter);

        btnData = (Button) findViewById(R.id.btndata);
        btnHora = (Button) findViewById(R.id.btnhora);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        tData = (TextView) findViewById(R.id.tData);
        tHora = (TextView) findViewById(R.id.tHora);
        titulo = (EditText) findViewById(R.id.titulo);
        nome_anexo = (EditText) findViewById(R.id.nome_anexo);
        descricao = (EditText) findViewById(R.id.descricao);

        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH);
        ano = calendar.get(Calendar.YEAR);

        tData.setText(dia + "/" + (mes + 1) + "/" + ano);

        hora = calendar.get(Calendar.HOUR_OF_DAY);
        minutos = calendar.get(Calendar.MINUTE);

        if (minutos < 10)
            tHora.setText(hora + ":0" + minutos);
        else
            tHora.setText(hora + ":" + minutos);

        /*Data e hora pickers*/
        btnData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog date = new DatePickerDialog(marcacao.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, datePickerListener, ano, mes, dia);
                date.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                date.show();
            }
        });

        datePickerListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int ano, int mes, int dia) {
                tData.setText(dia + "/" + (mes + 1) + "/" + ano);

                dia_escolhido = dia;
                mes_escolhido = (mes + 1);
                ano_escolhido = ano;
            }
        };

        btnHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog time = new TimePickerDialog(marcacao.this, timePickerListener, hora, minutos, true);
                time.show();
            }
        });

        timePickerListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hora, int minuto) {
                if (minuto < 10)
                    tHora.setText(hora + ":0" + minuto);
                else
                    tHora.setText(hora + ":" + minuto);

                hora_escolhida = hora;
                minutos_escolhidos = minuto;
            }
        };
        /*Data e hora pickers*/

        AdapterView.OnItemSelectedListener escolha = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = opcoes.getSelectedItem().toString();

                if (item.equals("Lista de Compras")) {
                    Intent lista = new Intent(marcacao.this, listaCompras.class);
                    startActivity(lista);
                } else if (item.equals("Reunião")) {
                    Intent marcacao = new Intent(marcacao.this, reuniao.class);
                    startActivity(marcacao);
                } else if (item.equals("Aniversário")) {
                    Intent aniversario = new Intent(marcacao.this, activity_aniversario.class);
                    startActivity(aniversario);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        opcoes.setOnItemSelectedListener(escolha);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (titulo.getText().toString().length() > 0) {
                    Tarefa marcacao = null;
                    if (dia_escolhido < dia || mes_escolhido < mes || ano_escolhido < ano) {
                        marcacao = new Tarefa(titulo.getText().toString(), tData.getText().toString(), tHora.getText().toString(), 0, 2, descricao.getText().toString(), 0);
                    } else if (dia_escolhido == dia && mes_escolhido == mes && ano_escolhido == ano) {
                        if (hora_escolhida > hora) {
                            marcacao = new Tarefa(titulo.getText().toString(), tData.getText().toString(), tHora.getText().toString(), 1, 2, descricao.getText().toString(), 0);
                        } else if (hora_escolhida == hora && minutos_escolhidos > minutos) {
                            marcacao = new Tarefa(titulo.getText().toString(), tData.getText().toString(), tHora.getText().toString(), 1, 2, descricao.getText().toString(), 0);
                        } else {
                            marcacao = new Tarefa(titulo.getText().toString(), tData.getText().toString(), tHora.getText().toString(), 0, 2, descricao.getText().toString(), 0);
                        }
                    } else {
                        marcacao = new Tarefa(titulo.getText().toString(), tData.getText().toString(), tHora.getText().toString(), 1, 2, descricao.getText().toString(), 0);
                    }

                    AgendaDBHandler db = new AgendaDBHandler(marcacao.this);
                    db.addTarefa(marcacao);

                    Toast.makeText(getApplicationContext(), "Tarefa criada com sucesso", Toast.LENGTH_SHORT).show();

                    Intent calendario = new Intent(marcacao.this, MainActivity.class);
                    startActivity(calendario);
                } else {
                    Toast.makeText(getApplicationContext(), "Introduza o titulo", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

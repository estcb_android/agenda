package com.ipcb.joana.calendario;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class marcacaoEditar extends AppCompatActivity {
    private EditText titulo, nome_anexo, descricao;
    private Button btnData, btnHora, btnGuardar, btnEliminar;
    private TextView tData, tHora;
    private int dia, mes, ano, hora, minutos, dia_escolhido, mes_escolhido, ano_escolhido, hora_escolhida, minutos_escolhidos;
    private Calendar calendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener datePickerListener;
    private TimePickerDialog.OnTimeSetListener timePickerListener;
    private Tarefa tarefa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marcacao_editar);

        btnData = (Button) findViewById(R.id.btndata);
        btnHora = (Button) findViewById(R.id.btnhora);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        tData = (TextView) findViewById(R.id.tData);
        tHora = (TextView) findViewById(R.id.tHora);
        titulo = (EditText) findViewById(R.id.titulo);
        nome_anexo = (EditText) findViewById(R.id.nome_anexo);
        descricao = (EditText) findViewById(R.id.descricao);
        btnEliminar = (Button) findViewById(R.id.btnEliminar);

        mostrarDados();

        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH);
        ano = calendar.get(Calendar.YEAR);

        hora = calendar.get(Calendar.HOUR_OF_DAY);
        minutos = calendar.get(Calendar.MINUTE);

        /*Data e hora pickers*/
        btnData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog date = new DatePickerDialog(marcacaoEditar.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, datePickerListener, ano, mes, dia);
                date.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                date.show();
            }
        });

        datePickerListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int ano, int mes, int dia) {
                tData.setText(dia + "/" + (mes + 1) + "/" + ano);

                dia_escolhido = dia;
                mes_escolhido = (mes + 1);
                ano_escolhido = ano;
            }
        };

        btnHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog time = new TimePickerDialog(marcacaoEditar.this, timePickerListener, hora, minutos, true);
                time.show();
            }
        });

        timePickerListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hora, int minuto) {
                if (minuto < 10)
                    tHora.setText(hora + ":0" + minuto);
                else
                    tHora.setText(hora + ":" + minuto);

                hora_escolhida = hora;
                minutos_escolhidos = minuto;
            }
        };
        /*Data e hora pickers*/

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(titulo.getText().toString().length() > 0) {
                    tarefa.setTitulo(titulo.getText().toString());
                    tarefa.setData(tData.getText().toString());
                    tarefa.setHora(tHora.getText().toString());
                    tarefa.setDescricao(descricao.getText().toString());

                    AgendaDBHandler db = new AgendaDBHandler(marcacaoEditar.this);
                    db.updateTarefa(tarefa);

                    Toast.makeText(getApplicationContext(), "Tarefa editada com sucesso", Toast.LENGTH_SHORT).show();

                    Intent calendario = new Intent(marcacaoEditar.this, MainActivity.class);
                    startActivity(calendario);
                } else {
                    Toast.makeText(getApplicationContext(), "Introduza o titulo", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgendaDBHandler db = new AgendaDBHandler(marcacaoEditar.this);
                db.deleteTarefa(tarefa);

                Toast.makeText(getApplicationContext(), "Tarefa eliminada com sucesso", Toast.LENGTH_SHORT).show();

                Intent calendario = new Intent(marcacaoEditar.this, MainActivity.class);
                startActivity(calendario);
            }
        });
    }

    private void mostrarDados() {
        Bundle extrasBundle = getIntent().getExtras();

        if (extrasBundle != null && !extrasBundle.isEmpty()) {
            tarefa = new AgendaDBHandler(marcacaoEditar.this).getTarefa(extrasBundle.getInt("idTarefa"), 2);

            String Titulo = tarefa.getTitulo();

            titulo.setText(Titulo);
            tData.setText(tarefa.getData());
            tHora.setText(tarefa.getHora());
            descricao.setText(tarefa.getDescricao());
        }

    }
}

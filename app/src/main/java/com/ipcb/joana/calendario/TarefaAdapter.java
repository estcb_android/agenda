package com.ipcb.joana.calendario;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

public class TarefaAdapter extends ArrayAdapter<Tarefa> implements CompoundButton.OnCheckedChangeListener {
    private SparseBooleanArray mCheckStates;
    private ArrayList<Tarefa> tarefas;
    private Context mContext;
    private Tarefa tarefa;

    static class TarefaInfoHolder
    {
        TextView nomeTarefa;
        TextView horaTarefa;
        Button btnEditar;
    }


    public TarefaAdapter(ArrayList<Tarefa> data, Context context) {
        super(context, R.layout.item_tarefa, data);
        this.tarefas = data;
        this.mContext = context;

        mCheckStates = new SparseBooleanArray(data.size());

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        tarefa = getItem(position);

        TarefaInfoHolder holder = null;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_tarefa, parent, false);

            holder = new TarefaInfoHolder();

            holder.nomeTarefa = (TextView) convertView.findViewById(R.id.nomeTarefa);
            holder.horaTarefa = (TextView) convertView.findViewById(R.id.horaTarefa);
            holder.btnEditar = (Button) convertView.findViewById(R.id.btnEditar);
            holder.btnEditar.setTag(position);

            convertView.setTag(holder);
        } else {
            holder = (TarefaInfoHolder) convertView.getTag();
        }


        holder.nomeTarefa.setText(tarefa.getTitulo());
        holder.horaTarefa.setText(tarefa.getHora());

        holder.btnEditar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                switch (tarefa.getTipo()) {
                    case 0:
                        Intent reuniao = new Intent(mContext, reuniaoEditar.class);
                        reuniao.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        reuniao.putExtra("idTarefa", tarefa.getId());

                        mContext.startActivity(reuniao);

                        break;

                    case 1:
                        Intent lista = new Intent(mContext, listaComprasEditar.class);
                        lista.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        lista.putExtra("idTarefa", tarefa.getId());

                        mContext.startActivity(lista);

                        break;

                    case 2:
                        Intent marcacao = new Intent(mContext, marcacaoEditar.class);
                        marcacao.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        marcacao.putExtra("idTarefa", tarefa.getId());

                        mContext.startActivity(marcacao);

                        break;

                    case 3:
                        Intent aniversario = new Intent(mContext, activity_aniversario_editar.class);
                        aniversario.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        aniversario.putExtra("idTarefa", tarefa.getId());

                        mContext.startActivity(aniversario);

                        break;

                    default:

                        break;
                }
            }
        });

        return convertView;
    }

    public boolean isChecked(int position) {
        return mCheckStates.get(position, false);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        mCheckStates.put((Integer) compoundButton.getTag(), b);
    }
}

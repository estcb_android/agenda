package com.ipcb.joana.calendario;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Tarefa> tarefas;
    private ListView tarefasHoje;
    private TextView dataHoje;
    private TarefaAdapter adapter;
    private AgendaDBHandler dbTarefas;

    private FloatingActionButton addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendario);

        mostrarDataHoje();

        mostrarTarefasHoje();

        //enviarMensagem();

        addButton = (FloatingActionButton) findViewById(R.id.addButton);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tipoEvento = new Intent(MainActivity.this, tipoEvento.class);
                startActivity(tipoEvento);
            }
        });
    }

    private void updateListView() {
        adapter = new TarefaAdapter(tarefas,getApplicationContext());
        adapter.notifyDataSetChanged();
        tarefasHoje.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        tarefas = dbTarefas.getHojeTarefas();
        updateListView();

        super.onResume();
    }

    public void mostrarDataHoje(){
        Calendar hoje = Calendar.getInstance();
        String nomeMes = "";

        dataHoje = (TextView) findViewById(R.id.dataHoje);

        switch (hoje.get(Calendar.MONTH)) {
            case 0:
                nomeMes = "Janeiro";
                break;
            case 1:
                nomeMes = "Fevereiro";
                break;
            case 2:
                nomeMes = "Março";
                break;
            case 3:
                nomeMes = "Abril";
                break;
            case 4:
                nomeMes = "Maio";
                break;
            case 5:
                nomeMes = "Junho";
                break;
            case 6:
                nomeMes = "Julho";
                break;
            case 7:
                nomeMes = "Agosto";
                break;
            case 8:
                nomeMes = "Setembro";
                break;
            case 9:
                nomeMes = "Outubro";
                break;
            case 10:
                nomeMes = "Novembro";
                break;
            case 11:
                nomeMes = "Dezembro";
                break;
        }

        dataHoje.setText(hoje.get(Calendar.DAY_OF_MONTH)+" de "+nomeMes+" de "+hoje.get(Calendar.YEAR));
    }

    public void mostrarTarefasHoje(){
        dbTarefas = new AgendaDBHandler(this);

        tarefasHoje = (ListView) findViewById(R.id.tarefasHoje);

        tarefas = new ArrayList<>();

        tarefas = dbTarefas.getHojeTarefas();

        adapter = new TarefaAdapter(tarefas, getApplicationContext());

        tarefasHoje.setAdapter(adapter);
    }

    public void enviarMensagem(){
        tarefas = new ArrayList<>();

        tarefas = dbTarefas.getHojeAniversarios();

        for(int i = 0; i < tarefas.size(); i++){
            Calendar calendar = Calendar.getInstance();

            String horaCompleta = tarefas.get(i).getHora();
            String[] horasplit = horaCompleta.split(":");
            int hora = Integer.parseInt(horasplit[0]);
            int minutos = 0;
            if(Integer.parseInt(horasplit[1]) < 10)
                minutos = Integer.parseInt(horasplit[1].split("")[2]);
            else
                minutos = Integer.parseInt(horasplit[1]);

            if(calendar.get(Calendar.HOUR_OF_DAY) >= hora &&calendar.get(Calendar.MINUTE) >= minutos && tarefas.get(i).getContacto() != 0) {

                calendar.set(Calendar.HOUR_OF_DAY, hora);
                calendar.set(Calendar.MINUTE, minutos);
                calendar.set(Calendar.SECOND, 0);

                Intent intent = new Intent(getApplicationContext(), activityEnviarMensagem.class);
                intent.putExtra("contacto", tarefas.get(i).getContacto());
                intent.putExtra("mensagem", tarefas.get(i).getMensagem());
                intent.setAction("MY_NOTIFICATION_MESSAGE");
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 100, intent, 0);

                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
            }
        }
    }
}

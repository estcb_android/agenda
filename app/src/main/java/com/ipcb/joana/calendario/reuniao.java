package com.ipcb.joana.calendario;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class reuniao extends AppCompatActivity {

    private static final int REQUEST_CODE = 43;

    private EditText titulo, nome_anexo, descricao;
    private Spinner opcoes;
    private Button btnData, btnHora, btnGuardar, btnProcurar;
    private TextView tData, tHora;
    private int dia, mes, ano, hora, minutos, dia_escolhido, mes_escolhido, ano_escolhido, hora_escolhida, minutos_escolhidos;
    private Calendar calendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener datePickerListener;
    private TimePickerDialog.OnTimeSetListener timePickerListener;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reuniao);

        opcoes = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.opcoes_reuniaolayout, android.R.layout.simple_spinner_item);
        opcoes.setAdapter(adapter);

        btnData = (Button) findViewById(R.id.btndata);
        btnHora = (Button) findViewById(R.id.btnhora);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        tData = (TextView) findViewById(R.id.tData);
        tHora = (TextView) findViewById(R.id.tHora);
        titulo = (EditText) findViewById(R.id.titulo);
        nome_anexo = (EditText) findViewById(R.id.nome_anexo);
        descricao = (EditText) findViewById(R.id.descricao);
        btnProcurar = (Button) findViewById(R.id.btnProcurar);

        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH);
        ano = calendar.get(Calendar.YEAR);

        tData.setText(dia + "/" + (mes+1) + "/" + ano);

        hora = calendar.get(Calendar.HOUR_OF_DAY);
        minutos = calendar.get(Calendar.MINUTE);

        if (minutos < 10)
            tHora.setText(hora + ":0" + minutos);
        else
            tHora.setText(hora + ":" + minutos);

        /*Data e hora pickers*/
        btnData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog date = new DatePickerDialog(reuniao.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, datePickerListener, ano, mes, dia);
                date.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                date.show();
            }
        });

        datePickerListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int ano, int mes, int dia) {
                tData.setText(dia + "/" + (mes + 1) + "/" + ano);

                dia_escolhido = dia;
                mes_escolhido = (mes + 1);
                ano_escolhido = ano;
            }
        };

        btnHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog time = new TimePickerDialog(reuniao.this, timePickerListener, hora, minutos, true);
                time.show();
            }
        });

        timePickerListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hora, int minuto) {
                if (minuto < 10)
                    tHora.setText(hora + ":0" + minuto);
                else
                    tHora.setText(hora + ":" + minuto);

                hora_escolhida = hora;
                minutos_escolhidos = minuto;
            }
        };
        /*Data e hora pickers*/

        AdapterView.OnItemSelectedListener escolha = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = opcoes.getSelectedItem().toString();

                if(item.equals("Lista de Compras")){
                    Intent lista = new Intent(reuniao.this, listaCompras.class);
                    startActivity(lista);
                }else if(item.equals("Marcação")){
                    Intent marcacao = new Intent(reuniao.this, marcacao.class);
                    startActivity(marcacao);
                }else if(item.equals("Aniversário")){
                    Intent aniversario = new Intent(reuniao.this, activity_aniversario.class);
                    startActivity(aniversario);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        opcoes.setOnItemSelectedListener(escolha);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (titulo.getText().toString().length() > 0) {
                    Tarefa reuniao = null;
                    if (dia_escolhido < dia || mes_escolhido < mes || ano_escolhido < ano) {
                        reuniao = new Tarefa(titulo.getText().toString(), tData.getText().toString(), tHora.getText().toString(), 0, 0, descricao.getText().toString(), path);
                    } else if (dia_escolhido == dia && mes_escolhido == mes && ano_escolhido == ano) {
                        if (hora_escolhida > hora) {
                            reuniao = new Tarefa(titulo.getText().toString(), tData.getText().toString(), tHora.getText().toString(), 1, 0, descricao.getText().toString(), path);
                        } else if (hora_escolhida == hora && minutos_escolhidos > minutos) {
                            reuniao = new Tarefa(titulo.getText().toString(), tData.getText().toString(), tHora.getText().toString(), 1, 0, descricao.getText().toString(), path);
                        } else {
                            reuniao = new Tarefa(titulo.getText().toString(), tData.getText().toString(), tHora.getText().toString(), 0, 0, descricao.getText().toString(), path);
                        }
                    } else {
                        reuniao = new Tarefa(titulo.getText().toString(), tData.getText().toString(), tHora.getText().toString(), 1, 0, descricao.getText().toString(), path);
                    }

                    AgendaDBHandler db = new AgendaDBHandler(reuniao.this);
                    db.addTarefa(reuniao);

                    Toast.makeText(getApplicationContext(), "Tarefa criada com sucesso", Toast.LENGTH_SHORT).show();

                    Intent calendario = new Intent(reuniao.this, MainActivity.class);
                    startActivity(calendario);
                } else {
                    Toast.makeText(getApplicationContext(), "Introduza o titulo", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnProcurar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent procurar = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                procurar.setType("application/pdf");
                procurar.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(procurar, REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK){
            if(data != null){
                Uri uri = data.getData();

                path = uri.getPath();

                String filename = uri.getPath().substring(uri.getPath().lastIndexOf("/")+1);
                nome_anexo.setText(filename);
            }
        }
    }
}